package com.pruebatecnica.backend.controller;

import com.pruebatecnica.backend.entity.Estudiante;
import com.pruebatecnica.backend.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/estudiante"})

public class ControladorEstudiante {
    @Autowired
    public EstudianteService service;

    @GetMapping
    public List<Estudiante>listar(){
        return service.listar();
    }

    @PostMapping
    public Estudiante agregar(@RequestBody Estudiante e){
        return service.agregar(e);
    }
}
