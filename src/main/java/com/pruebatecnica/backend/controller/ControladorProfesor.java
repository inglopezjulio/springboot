package com.pruebatecnica.backend.controller;

import com.pruebatecnica.backend.entity.Profesor;
import com.pruebatecnica.backend.service.ProfesorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/profesor"})

public class ControladorProfesor {
    @Autowired
    public ProfesorService service;

    @GetMapping
    public List<Profesor>listar(){
        return service.listar();
    }

    @PostMapping
    public Profesor agregar(@RequestBody Profesor p){
        return service.agregar(p);
    }
}
