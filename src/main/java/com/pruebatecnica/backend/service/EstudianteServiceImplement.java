package com.pruebatecnica.backend.service;

import com.pruebatecnica.backend.entity.Estudiante;
import com.pruebatecnica.backend.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class EstudianteServiceImplement implements EstudianteService {
    @Autowired
    private EstudianteRepository repository;

    @Override
    public List<Estudiante> listar() {
        return repository.findAll();
    }

    @Override
    public Estudiante agregar(Estudiante e) {
        return repository.save(e);
    }
}