package com.pruebatecnica.backend.service;

import com.pruebatecnica.backend.entity.Profesor;

import java.util.List;

public interface ProfesorService {
    List<Profesor>listar();
    Profesor agregar(Profesor p);
}
