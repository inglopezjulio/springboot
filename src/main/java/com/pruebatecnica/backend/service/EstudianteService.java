package com.pruebatecnica.backend.service;

import com.pruebatecnica.backend.entity.Estudiante;

import java.util.List;

public interface EstudianteService {
    List<Estudiante>listar();
    Estudiante agregar(Estudiante e);

}
