package com.pruebatecnica.backend.service;

import com.pruebatecnica.backend.entity.Profesor;
import com.pruebatecnica.backend.repository.ProfesorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class ProfesorServiceImplement implements ProfesorService {
    @Autowired
    private ProfesorRepository repository;

    @Override
    public List<Profesor> listar() {
        return repository.findAll();
    }

    @Override
    public Profesor agregar(Profesor p) {
        return repository.save(p);
    }

}