package com.pruebatecnica.backend.repository;

import com.pruebatecnica.backend.entity.Profesor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProfesorRepository extends CrudRepository<Profesor, Integer> {
    List<Profesor>findAll();

}
