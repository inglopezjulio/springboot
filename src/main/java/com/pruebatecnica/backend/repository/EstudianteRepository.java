package com.pruebatecnica.backend.repository;

import com.pruebatecnica.backend.entity.Estudiante;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteRepository extends CrudRepository<Estudiante, Integer> {
    List<Estudiante>findAll();
}
