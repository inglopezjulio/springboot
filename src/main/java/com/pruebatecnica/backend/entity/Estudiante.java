package com.pruebatecnica.backend.entity;

import javax.persistence.*;

@Entity
@Table(name="estudiantes")

public class Estudiante {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String nombres;

    @Column
    private String apellidos;

    @Column
    private int edad;

    @Column
    private Long tarjeta_identidad;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Long getTarjeta_identidad() {
        return tarjeta_identidad;
    }

    public void setTarjeta_identidad(Long tarjeta_identidad) {
        this.tarjeta_identidad = tarjeta_identidad;
    }


}
